package br.ufsc.lapesd.ws.agents.common_knowledge.endpoints;

import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.riot.RiotException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.charset.Charset;

@RestController
public class CommonKnowledgeEndpoint {
    Logger log = LoggerFactory.getLogger(CommonKnowledgeEndpoint.class);

    @Value("${modelPath}")
    private String modelPath;

    private Model model;

    @PostConstruct
    public void init() {
        model = ModelFactory.createDefaultModel();
        boolean loaded = false;
        try (InputStream inputStream = new FileInputStream(modelPath)) {
            String baseURI = "file://" + modelPath;
            model.read(inputStream, baseURI, getModelPathLang().getName());
            loaded = true;
        } catch (FileNotFoundException e) {
            log.info("model file not found");
        } catch (Exception e) {
            log.error("Problem loading model from " + modelPath, e);
        }

        if (!loaded) {
            String uris[] = {
                    "http://alexishuf.bitbucket.org/2016/07/us-agents/sd.ttl",
                    "http://alexishuf.bitbucket.org/2016/07/us-agents/usa.ttl",
                    "http://alexishuf.bitbucket.org/2016/07/us-agents/examples/security/ontology.ttl",
                    "http://alexishuf.bitbucket.org/2016/07/us-agents/examples/security/desires.ttl",
                    "http://alexishuf.bitbucket.org/2016/07/us-agents/examples/security/composite-desires.ttl"
            };
            for (String uri : uris) {
                try {
                    RDFDataMgr.read(model, uri, Lang.TURTLE);
                } catch (Exception e) {
                    log.error("Problem loading " + uri, e);
                }
            }
        }
        log.info("model.size()=" + model.size());
    }

    @RequestMapping(value = "/commonknowledge.ttl", produces = {"text/turtle"})
    public @ResponseBody String get() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        RDFDataMgr.write(outputStream, model, Lang.TURTLE);
        return new String(outputStream.toByteArray(), Charset.forName("UTF-8"));
    }

    @RequestMapping(value = "/commonknowledge.ttl", method = RequestMethod.POST,
            produces = {"text/turtle"})
    public @ResponseBody String post(@RequestBody String rdfString) {
//        model.read(IOUtils.toInputStream(rdfString), null, null);
        /* may god have mercy on my soul */
        for (Lang lang : RDFLanguages.getRegisteredLanguages()) {
            Model tmp = ModelFactory.createDefaultModel();
            try {
                RDFDataMgr.read(tmp, IOUtils.toInputStream(rdfString), lang);
                model.add(tmp);
                break;
            } catch (RiotException ignored) {
            }
        }
        saveModel();

        return get();
    }

    private void saveModel() {
        try (FileOutputStream outputStream = new FileOutputStream(modelPath)) {
            RDFDataMgr.write(outputStream, model, getModelPathLang());
        } catch (Exception e) {
            log.error("Problem writing model to " + modelPath, e);
        }
    }

    private Lang getModelPathLang() {
        String[] parts = modelPath.split("\\.");
        Lang lang = RDFLanguages.nameToLang(parts[parts.length - 1]);
        return lang != null ? lang : Lang.RDFXML;
    }
}
