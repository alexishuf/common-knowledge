package br.ufsc.lapesd.ws.agents.common_knowledge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.io.IOException;

@SpringBootApplication
@ComponentScan("br.ufsc.lapesd.ws.agents.common_knowledge")
public class WebApi {
    public static void main(String[] args) throws IOException {
        SpringApplication.run(WebApi.class, args);

    }
}
